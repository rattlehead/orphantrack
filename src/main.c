#include "main.h"

//===================== Function declarations =======================

static void init();
static int main_internal(int argc, char** args);
static void cleanup();
static int handle_args(int argc, char** args);

//===================== Variables ===================================

alpm_list_t* ignored;
alpm_list_t* special_ignored;
int option_interactive = FALSE;

//===================== Function implementations ====================

int main(int argc, char** args) {
    init();
    int ret_code = main_internal(argc, args);
    cleanup();
    return ret_code;
}

void init() {
}

int main_internal(int argc, char** args) {
    ASSERT(handle_args(argc, args) == OK)
    ASSERT(load_config() == OK)
    ASSERT(print_orphaned_files() == OK)
    return OK;
}

void cleanup() {
    FREELIST(ignored);
    FREELIST(special_ignored);
}

int handle_args(int argc, char** args) {
    int status = OK;
    char* allowed_flags = "i";

    for (int opt = getopt(argc, args, allowed_flags); opt != -1; opt = getopt(argc, args, allowed_flags)) {
        switch (opt) {
            case 'i':
                option_interactive = TRUE;
                break;
            case '?':
                status = ERROR;
                break;
            default:
                break;
        }
    }
    return status;
}