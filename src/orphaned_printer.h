#pragma once

#include <stdio.h>
#include <alpm.h>
#include <dirent.h>
#include <string.h>

#include "common.h"
#include "interactive_handler.h"

//===================== Constants ===================================

#define ALPM_DB_LOCATION "/var/lib/pacman/"

//===================== Macro definitions ===========================

#define SHOULD_PRINT(status)\
    (IS_UNOWNED(status) && !IS_IGNORED(status))
#define SHOULD_STEP_INTO(path, status)\
    (IS_DIRECTORY(path) && !IS_SKIPPED(status) && !IS_REMOVED(status)\
        && !SHOULD_PRINT(status) && !IS_RECURSIVELY_IGNORED(status))

//===================== Function declarations =======================

int print_orphaned_files();