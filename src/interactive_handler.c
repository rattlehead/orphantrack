#include "interactive_handler.h"

//===================== Function declarations =======================

static void print_parents_owners(char* path);
static alpm_list_t* get_owners(char *path);
static uint handle_user_input(char** options, char* keys, interactive_handler_t* handlers, int size, char* path);
static uint handle_remove(char* path);
static uint handle_skip();
static uint handle_ignore(char* path);
static uint handle_cut_path(char* path);
static uint handle_abort();
static uint handle_normal_ignore(char* path);
static uint handle_normal_self_ignore(char* path);
static uint handle_normal_recursive_ignore(char* path);
static uint handle_special_ignore(char* path);
static uint handle_special_self_ignore(char* path);
static uint handle_special_recursive_ignore(char* path);
static void add_ignored(char* path);
static void add_special_ignored(char* path);

//===================== Variables ===================================

extern alpm_db_t* local_db;
extern alpm_list_t* ignored;
extern alpm_list_t* special_ignored;
extern int is_config_modified;
extern int is_aborted;

//===================== Function implementations ====================

uint interactive_handle(char* path) {
    printf("=== Found unowned entry: %s\n", path);
    print_parents_owners(path);
    printf("Make your choice about %s:\n", path);

    char* options[] = { "remove", "skip", "ignore", "abort" };
    char keys[] = { 'r', 's', 'i', 'a' };
    interactive_handler_t handlers[] = { handle_remove, handle_skip, handle_ignore, handle_abort };
    return handle_user_input(options, keys, handlers, 4, path);
}

void print_parents_owners(char* path) {
    char path_copy[BUFFER_SIZE];
    strcpy(path_copy, path);

    if (IS_DIRECTORY(path_copy)) {
        LAST_CHAR(path_copy) = 0;
    }

    for (*(strrchr(path_copy, '/') + 1) = 0; strlen(path_copy) > 1; *(strrchr(path_copy, '/') + 1) = 0) {
        alpm_list_t* owners = get_owners(path_copy);
        if (alpm_list_count(owners) > 0) {
            for (alpm_list_t* iter = owners; iter != NULL; iter = alpm_list_next(iter)) {
                printf("==== Parent path %s is owned by %s\n", path_copy, (char*) iter->data);
            }
        } else {
            printf("==== Parent path %s is unowned\n", path_copy);
        }
        alpm_list_free(owners);
        LAST_CHAR(path_copy) = 0;
    }
}

alpm_list_t* get_owners(char *path) {
    alpm_list_t* owners = NULL;
    for (alpm_list_t* p = alpm_db_get_pkgcache(local_db); p != NULL; p = p->next) {
        if (alpm_filelist_contains(alpm_pkg_get_files(p->data), path + 1)) {
            owners = alpm_list_add(owners, (void*) alpm_pkg_get_name(p->data));
        }
    }
    return owners;
}

uint handle_user_input(char** options, char* keys, interactive_handler_t* handlers, int size, char* path) {
    while (TRUE) {
        printf("Options - ");
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                printf(", ");
            }
            printf("(%c) %s", keys[i], options[i]);
        }
        printf(": ");
        int input = getchar();
        getchar();
        putc('\n', stdout);

        for (int i = 0; i < size; i++) {
            if (input == keys[i]) {
                return handlers[i](path);
            }
        }
    }
}

uint handle_remove(char *path) {
    char remove_command[BUFFER_SIZE];
    strcpy(remove_command, "rm -rf ");
    strcat(remove_command, path);
    system(remove_command);
    return REMOVED;
}

uint handle_skip() {
    return SKIPPED;
}

uint handle_ignore(char *path) {
    puts(path);
    char* options[] = { "normal ignore", "special ignore", "cut", "abort" };
    char keys[] = { 'n', 's', 'c', 'a' };
    interactive_handler_t handlers[] = { handle_normal_ignore, handle_special_ignore, handle_cut_path, handle_abort };
    return handle_user_input(options, keys, handlers, 4, path);
}

uint handle_cut_path(char *path) {
    if (STRING_COMPARE(path, ROOT_DIRECTORY)) {
        return handle_ignore(path);
    }

    char path_copy[BUFFER_SIZE];
    strcpy(path_copy, path);

    if (IS_DIRECTORY(path_copy)) {
        LAST_CHAR(path_copy) = 0;
    }
    *(strrchr(path_copy, '/') + 1) = 0;
    return handle_ignore(path_copy);
}

uint handle_abort() {
    is_aborted = TRUE;
    return 0;
}

uint handle_normal_ignore(char* path) {
    if (IS_DIRECTORY(path)) {
        char* options[] = { "self ignore", "recursive ignore" };
        char keys[] = { 's', 'r' };
        interactive_handler_t handlers[] = { handle_normal_self_ignore, handle_normal_recursive_ignore };
        return handle_user_input(options, keys, handlers, 2, path);
    }
    return handle_normal_self_ignore(path);
}

uint handle_special_ignore(char* path) {
    if (IS_DIRECTORY(path)) {
        char* options[] = { "self ignore", "recursive ignore" };
        char keys[] = { 's', 'r' };
        interactive_handler_t handlers[] = { handle_special_self_ignore, handle_special_recursive_ignore };
        return handle_user_input(options, keys, handlers, 2, path);
    }
    return handle_special_self_ignore(path);
}

uint handle_normal_self_ignore(char* path) {
    char* path_copy = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(path_copy, path);
    if (IS_DIRECTORY(path_copy)) {
        LAST_CHAR(path_copy) = 0;
    }
    add_ignored(path_copy);
    return IGNORED;
}

uint handle_normal_recursive_ignore(char* path) {
    char* path_copy = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(path_copy, path);
    add_ignored(path_copy);
    return RECURSIVELY_IGNORED;
}

uint handle_special_self_ignore(char* path) {
    char* path_copy = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(path_copy, path);
    if (IS_DIRECTORY(path_copy)) {
        LAST_CHAR(path_copy) = 0;
    }
    add_special_ignored(path_copy);
    return IGNORED;
}

uint handle_special_recursive_ignore(char* path) {
    char* path_copy = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(path_copy, path);
    add_special_ignored(path_copy);
    return RECURSIVELY_IGNORED;
}

void add_ignored(char *path) {
    ignored = alpm_list_add(ignored, path);
    is_config_modified = TRUE;
}

void add_special_ignored(char *path) {
    char* package = calloc(BUFFER_SIZE, sizeof(char));
    size_t buffer_size = BUFFER_SIZE;
    puts("Specify a package to which this path should be bound:");
    getline(&package, &buffer_size, stdin);
    LAST_CHAR(package) = 0;

    special_ignored_t* item = malloc(sizeof(special_ignored_t));
    item->path = path;
    item->package = package;

    special_ignored = alpm_list_add(special_ignored, item);
    is_config_modified = TRUE;
}