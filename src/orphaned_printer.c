#include "orphaned_printer.h"

//===================== Function declarations =======================

static void init();
static int print_orphaned_files_internal();
static void cleanup();
static int scan_recursive(char* dir);
static uint get_path_status(char* path);
static int is_unowned(char* path);
static uint get_ignored_match_status(char* path, char* ignored_path);
static int update_config();

//===================== Variables ===================================

extern alpm_list_t* ignored;
extern alpm_list_t* special_ignored;
extern int option_interactive;

static alpm_handle_t* handle;

alpm_db_t* local_db;
int is_config_modified = FALSE;
int is_aborted = FALSE;

//===================== Function implementations ====================

int print_orphaned_files() {
    init();
    int ret_code = print_orphaned_files_internal();
    cleanup();
    return ret_code;
}

void init() {
    handle = alpm_initialize(ROOT_DIRECTORY, ALPM_DB_LOCATION, NULL);
    local_db = alpm_get_localdb(handle);
}

int print_orphaned_files_internal() {
    ASSERT(scan_recursive(ROOT_DIRECTORY) == OK)
    if (is_config_modified) {
        ASSERT(update_config() == OK)
    }
    return OK;
}

void cleanup() {
    alpm_release(handle);
}

int scan_recursive(char* dir) {
    DIR* directory = opendir(dir);
    ASSERT_LOG_FORMATTED(directory != NULL, "Error: error opening directory %s", dir);

    char path[BUFFER_SIZE];
    strcpy(path, dir);
    char* filename = path + strlen(dir);

    struct dirent* entry;
    while ((entry = readdir(directory))) {
        if (is_aborted) {
            break;
        }

        if (STRING_COMPARE(entry->d_name, ".") || STRING_COMPARE(entry->d_name, "..")) {
            continue;
        }

        strcpy(filename, entry->d_name);
        if (entry->d_type == DT_DIR) {
            strcat(path, "/");
        }

        uint status = get_path_status(path);

        if (option_interactive && SHOULD_PRINT(status)) {
            system("clear");
            status = status | interactive_handle(path);
        } else if (SHOULD_PRINT(status)) {
            puts(path);
        }

        if (SHOULD_STEP_INTO(path, status)) {
            scan_recursive(path);
        }
    }

    closedir(directory);
    return OK;
}

uint get_path_status(char* path) {
    uint result = 0;

    if (is_unowned(path)) {
        result = result | UNOWNED;
    }

    for (alpm_list_t* iter = ignored; iter != NULL; iter = alpm_list_next(iter)) {
        result = result | get_ignored_match_status(path, iter->data);
    }
    for (alpm_list_t* iter = special_ignored; iter != NULL; iter = alpm_list_next(iter)) {
        special_ignored_t* special_ignored_item = iter->data;
        if (alpm_db_get_pkg(local_db, special_ignored_item->package) != NULL) {
            result = result | get_ignored_match_status(path, special_ignored_item->path);
        }
    }

    return result;
}

int is_unowned(char* path) {
    for (alpm_list_t* p = alpm_db_get_pkgcache(local_db); p != NULL; p = p->next) {
        if (alpm_filelist_contains(alpm_pkg_get_files(p->data), path + 1)) {
            return FALSE;
        }
    }
    return TRUE;
}

uint get_ignored_match_status(char* path, char* ignored_path) {
    if (LAST_CHAR(ignored_path) == '*') {
        if (STRING_N_COMPARE(path, ignored_path, strlen(ignored_path) - 1)) {
            return IS_DIRECTORY(path) ? RECURSIVELY_IGNORED : IGNORED;
        }
    }

    if (IS_DIRECTORY(path)) {
        if (STRING_COMPARE(path, ignored_path)) {
            return RECURSIVELY_IGNORED;
        }
        if (STRING_N_COMPARE(path, ignored_path, strlen(path) - 1)) {
            return IGNORED;
        }
    } else {
        return STRING_COMPARE(path, ignored_path) ? IGNORED : 0;
    }
    return 0;
}

int update_config() {
    char copy_command[BUFFER_SIZE];
    strcpy(copy_command, "cp ");
    strcat(copy_command, CONFIG_LOCATION);
    strcat(copy_command, " ");
    strcat(copy_command, CONFIG_BACKUP_LOCATION);
    system(copy_command);

    FILE* config_file = fopen(CONFIG_LOCATION, "w");
    ASSERT_LOG(config_file != NULL,
               "Error: configuration file /etc/orphantrack.conf was not found")

    fprintf(config_file, "%s\n", IGNORE_HEADER);
    for (alpm_list_t* iter = ignored; iter != NULL; iter = alpm_list_next(iter)) {
        fprintf(config_file, "%s\n", (char*) iter->data);
    }

    fprintf(config_file, "\n%s\n", SPECIAL_IGNORE_HEADER);
    for (alpm_list_t* iter = special_ignored; iter != NULL; iter = alpm_list_next(iter)) {
        special_ignored_t* item = iter->data;
        fprintf(config_file, "%s = %s\n", item->package, item->path);
    }
    fclose(config_file);
    return OK;
}