#pragma once

#include <stdio.h>
#include <regex.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>
#include <alpm_list.h>

#include "common.h"

//===================== Type definitions=============================

typedef int(*ignore_handler_t)(char*);

//===================== Function declarations =======================

int load_config();