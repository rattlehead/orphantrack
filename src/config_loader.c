#include "config_loader.h"

//===================== Function declarations =======================

static void init();
static int load_config_internal();
static void cleanup();
static int parse_config(FILE* file);
static void trim_last(char* text);
static ignore_handler_t get_handler(char* header);
static int update_ignored(char* line);
static int update_special_ignored(char* line);

//===================== Variables ===================================

extern alpm_list_t* ignored;
extern alpm_list_t* special_ignored;

static regex_t header_regex;
static regex_t comment_or_blank_regex;
static regex_t special_ignore_regex;

//===================== Function implementations ====================

int load_config() {
    init();
    int ret_code = load_config_internal();
    cleanup();
    return ret_code;
}

void init() {
    regcomp(&header_regex, "^\\[[a-zA-Z]+\\]$", REG_EXTENDED);
    regcomp(&comment_or_blank_regex, "(^\\s*$)|(^#.*$)", REG_EXTENDED);
    regcomp(&special_ignore_regex, "^\\s*(.+)\\s*=\\s*(.+)\\s*$", REG_EXTENDED);
}

int load_config_internal() {
    FILE* config_file = fopen(CONFIG_LOCATION, "r");
    ASSERT_LOG(config_file != NULL,
               "Error: configuration file /etc/orphantrack.conf was not found")
    ASSERT_LOG(parse_config(config_file) == OK,
               "Error: configuration file /etc/orphantrack.conf is broken")
    fclose(config_file);
    return OK;
}

void cleanup() {
    regfree(&header_regex);
    regfree(&comment_or_blank_regex);
    regfree(&special_ignore_regex);
}

int parse_config(FILE* file) {
    char line[BUFFER_SIZE];
    ignore_handler_t handler = NULL;

    while (fgets(line, BUFFER_SIZE, file)) {
        trim_last(line);

        if (regexec(&comment_or_blank_regex, line, 0, NULL, 0) == 0) {
            continue;
        } else if (regexec(&header_regex, line, 0, NULL, 0) == 0) {
            handler = get_handler(line);
        } else if (handler == NULL) {
            return ERROR;
        } else {
            handler(line);
        }
    }

    return OK;
}

void trim_last(char* text) {
    for (size_t i = strlen(text) - 1; i >= 0; i--) {
        if (isspace(text[i])) {
            text[i] = 0;
        } else {
            break;
        }
    }
}

ignore_handler_t get_handler(char* header) {
    if (STRING_COMPARE(header, IGNORE_HEADER)) {
        return update_ignored;
    } else if (STRING_COMPARE(header, SPECIAL_IGNORE_HEADER)) {
        return update_special_ignored;
    }

    return NULL;
}

int update_ignored(char* line) {
    char* line_copy = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(line_copy, line);
    ignored = alpm_list_add(ignored, line_copy);
    return OK;
}

int update_special_ignored(char* line) {
    regmatch_t group_array[3];
    ASSERT(regexec(&special_ignore_regex, line, 3, group_array, 0) == 0)

    char* package = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(package, line + group_array[1].rm_so);
    package[group_array[1].rm_eo - group_array[1].rm_so] = 0;
    trim_last(package);

    char* path = calloc(BUFFER_SIZE, sizeof(char));
    strcpy(path, line + group_array[2].rm_so);
    path[group_array[2].rm_eo - group_array[2].rm_so] = 0;
    trim_last(path);

    special_ignored_t* result = malloc(sizeof(special_ignored_t));
    result->package = package;
    result->path = path;

    special_ignored = alpm_list_add(special_ignored, result);
    return OK;
}
