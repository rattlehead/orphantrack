#pragma once

#include <stdio.h>
#include <string.h>
#include <alpm.h>

#include "common.h"

//===================== Type definitions=============================

typedef uint(*interactive_handler_t)(char*);

//===================== Function declarations =======================

uint interactive_handle(char* path);