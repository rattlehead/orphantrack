#pragma once

#include <stdio.h>
#include <malloc.h>
#include <alpm.h>

#include "common.h"
#include "config_loader.h"
#include "orphaned_printer.h"

//===================== Function declarations =======================

int main(int argc, char** args);