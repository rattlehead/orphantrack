#pragma once

//===================== Type definitions=============================

struct special_ignored_struct {
    char* package;
    char* path;
};

typedef struct special_ignored_struct special_ignored_t;

//===================== Constants ===================================

#define OK 0
#define ERROR 1
#define TRUE 1
#define FALSE 0
#define BUFFER_SIZE 256

#define ROOT_DIRECTORY "/"
#define CONFIG_LOCATION "/etc/orphantrack.conf"
#define CONFIG_BACKUP_LOCATION "/etc/orphantrack.conf.bak"

#define IGNORE_HEADER "[Ignore]"
#define SPECIAL_IGNORE_HEADER "[IgnoreWhilePackageInstalled]"

#define UNOWNED (uint) 0b00001
#define IGNORED (uint) 0b00010
#define RECURSIVELY_IGNORED (uint) 0b00110
#define REMOVED (uint) 0b01000
#define SKIPPED (uint) 0b10000

//===================== Macro definitions ===========================

#define IS_UNOWNED(status) ((status & UNOWNED) == UNOWNED)
#define IS_IGNORED(status) ((status & IGNORED) == IGNORED)
#define IS_RECURSIVELY_IGNORED(status) ((status & RECURSIVELY_IGNORED) == RECURSIVELY_IGNORED)
#define IS_REMOVED(status) ((status & REMOVED) == REMOVED)
#define IS_SKIPPED(status) ((status & SKIPPED) == SKIPPED)
#define IS_DIRECTORY(path) (LAST_CHAR(path) == '/')

#define LAST_CHAR(str) str[strlen(str) - 1]
#define STRING_COMPARE(str1, str2) (strcmp(str1, str2) == 0)
#define STRING_N_COMPARE(str1, str2, n) (strncmp(str1, str2, n) == 0)

#define ASSERT(condition)\
    if (!(condition)) {\
        return ERROR;\
    }

#define ASSERT_LOG(condition, message)\
    if (!(condition)) {\
        fputs(message, stderr);\
        fputc('\n', stderr);\
        return ERROR;\
    }

#define ASSERT_LOG_FORMATTED(condition, message, args...)\
    if (!(condition)) {\
        fprintf(stderr, message, args);\
        fputc('\n', stderr);\
        return ERROR;\
    }
