# ==========================================================================
# Executable filename
TARGET_NAME 		:= orphantrack

# Other filenames
CONFIG_NAME			:= orphantrack.conf

# GCC options
CC 					:= gcc
CFLAGS 				:= -Wall -O3
LIBS 				:= -lalpm

# Build directories
SRCDIR 				:= ./src/
OBJDIR 				:= ./obj/
TARGETDIR 			:= ./target/

# Target executable
TARGET 				:= $(TARGETDIR)$(TARGET_NAME)

# Installation directories
BINDIR				:= /usr/bin/
CONFIGDIR			:= /etc/

# ==========================================================================
# Default target rule
default_target: 	all

# Clear implicit rules suffixes
.SUFFIXES:

# Phony targets
.PHONY: 			all clean install uninstall

# Compilation rule
$(OBJDIR)%.o: 		$(SRCDIR)%.c
	$(CC) -c $(CFLAGS) $< -o $@

# ==========================================================================
$(OBJDIR):
	mkdir -p $(OBJDIR)

$(TARGETDIR):
	mkdir -p $(TARGETDIR)

$(TARGET): 			$(OBJDIR)main.o \
					$(OBJDIR)config_loader.o \
					$(OBJDIR)interactive_handler.o \
					$(OBJDIR)orphaned_printer.o
	$(CC) $(CFLAGS) $(LIBS) $^ -o $@

all: 				$(OBJDIR) $(TARGETDIR) $(TARGET)

clean:
	rm -rf $(TARGETDIR) $(OBJDIR)

install:			all
	mkdir -p $(DESTDIR)$(BINDIR) $(DESTDIR)$(CONFIGDIR)
	cp $(TARGET) $(DESTDIR)$(BINDIR)$(TARGET_NAME)
	touch $(DESTDIR)$(CONFIGDIR)$(CONFIG_NAME)

uninstall:
	rm $(DESTDIR)$(BINDIR)$(TARGET_NAME)
	rm $(DESTDIR)$(CONFIGDIR)$(CONFIG_NAME)